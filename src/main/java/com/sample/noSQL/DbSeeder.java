package com.sample.noSQL;

import java.io.File;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.sample.noSQL.Repository.AdminRepository;
import com.sample.noSQL.Repository.LogsRepository;
import com.sample.noSQL.model.AccessLog;
import com.sample.noSQL.model.Admin;
import com.sample.noSQL.model.Block;
import com.sample.noSQL.model.Destination;
import com.sample.noSQL.model.Logs;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;


@Component
public class DbSeeder implements CommandLineRunner{
	private LogsRepository logsRepository;
	private AdminRepository adminRepository;
	
	public DbSeeder(LogsRepository logsRepository, AdminRepository adminRepository) {
		this.logsRepository = logsRepository;
		this.adminRepository = adminRepository;
	}
	
	@Override
	public void run(String...strings) throws Exception {

		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");

		FindIterable<Document> docs = collection.find().limit(1);
		boolean init = false;

		if(docs.first()==null)
			init = true;

		mongoClient.close();
		if (init){
			System.out.println("Start Insert of Access");
			insert_accessLogs();
			System.out.println("End Insert of Access");
		
			System.out.println("Start Insert of DataXceiver");
			insert_HDFS_DataXceiver();
			System.out.println("End Insert of DataXceiver");

			System.out.println("Start Insert of Namesystem");
			insert_HDFS_FS_Namesystem();
			System.out.println("End Insert of Namesystem");

			System.out.println("Start Create of Admins");
			create_admins();
			System.out.println("End Create of Admins");
		}
}
	

	private Integer create_admins(){
		Random rand = new Random();

		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");


		FindIterable<Document> findIterable = collection.find().projection(Projections.include("_id"));
		List<ObjectId> ids = new ArrayList<ObjectId>();
		List<String> reusedEmails = new ArrayList<String>();
 		for (Document doc : findIterable) {
          ids.add(new ObjectId(doc.get("_id").toString()));
        }
		findIterable = null;
		mongoClient.close();
		
		Collections.shuffle(ids);
		int count = 0, size = ids.size(), minimumSize;
		minimumSize = size/3;
		while (count < minimumSize)
		{
			List<ObjectId> idsList = new ArrayList<ObjectId>();

			int i;
			int selected;
				selected= rand.nextInt(1001);
			if(selected>size-count)
				selected=size-count;

			for (i=0; i<selected; i++) {
				idsList.add(ids.get(count));
				count++;
				if(count==ids.size())
					break;
			}

			Admin admin = new Admin(idsList);
			if(rand.nextInt(100)==13)
				reusedEmails.add(admin.getEmail());

	 		try {
	 			this.adminRepository.save(admin);
	 			count = count - rand.nextInt(selected);
	 		} catch(Exception e){
	 			count-=selected;
	 			if(count<0)
	 				count=0;
	 		}
		}
				
		for(String mail:reusedEmails) {
			count = 0;
			Collections.shuffle(ids);
			List<ObjectId> idsList = new ArrayList<ObjectId>();

			int i;
			int selected;
				selected= rand.nextInt(1001);
			if(selected>size)
				selected=size;

			for (i=0; i<selected; i++) {
				idsList.add(ids.get(count));
				count++;
			}

			Admin admin = new Admin(idsList);
			admin.setEmail(mail);
	 		try {
	 			this.adminRepository.save(admin);
	 		} catch(Exception e){
	 			
	 		}

		}
		return 0;
	}
	
	private Integer insert_accessLogs()	{
		
		List<Logs> logs = new ArrayList<Logs>();
		AccessLog acc;
		Logs log;
 		String s = ""; 		
 		String type="access", ipAddress, user_id, method, resource, referer, user_agent;
		Integer response, size;
		
 		int count = 0;
	 	File file = new File("C:\\Users\\User\\Documents\\workspace-spring-tool-suite-4-4.3.2.RELEASE\\M149\\files\\access.log"); 
	 	try{
	 		Scanner sc = new Scanner(file);
	 		String[] parts, parts2;
	 		while (sc.hasNextLine())
	 		{
	 			s = sc.nextLine();
	 			try {
	 		 		parts = s.split(" - ", 2);
		            parts2=parts[0].split(":", 2);
		            ipAddress = parts2[0];
		 			parts = parts[1].split(" ", 2);
		 			user_id=parts[0];
		 			parts = parts[1].split("] \"", 2);

		 			String tempTime=parts[0].substring(1);
		 			parts2 = tempTime.split("/", 2);
		 			String day= parts2[0];
		 			parts2 = parts2[1].split("/", 2);
		 			String month = parts2[0];
		 			parts2 = parts2[1].split(":", 2);
		 			String year= parts2[0];
		 			parts2 = parts2[1].split(":", 2);
		 			String hours= parts2[0];
		 			parts2 = parts2[1].split(":", 2);
		 			String minutes= parts2[0];
		 			parts2 = parts2[1].split(" ", 2);
		 			String seconds= parts2[0];


		 			long timestamp = Long.parseLong((year+getMonthNum(month)+day+hours+minutes+seconds));
		 			parts = parts[1].split(" ", 2);
		 			method = parts[0];
		 			String temp = parts[1];
		 			parts = parts[1].split("\" \\d+", 2);
		 			String pt1 = parts[0].trim();
		 		    String pt2 = temp.substring(pt1.length() + 1).trim();
		 			parts2 = parts[0].split(" ", 2);
		 			resource = parts2[0];

		 			parts = pt2.split(" ", 2);
		 			response = Integer.parseInt(parts[0]);
		 			parts = parts[1].split(" \"", 2);
		 			if(parts[0].equals("-"))
		 				size = null;
		 			else	
		 				size = Integer.parseInt(parts[0]);
		 			parts = parts[1].split("\" \"", 2);
		 			
		 			if(parts[0].length()==0 || parts[0].equals("-"))
		 				parts[0]=null;

		 			referer = parts[0];
		 			parts = parts[1].split("\"", 2);
		 			user_agent = parts[0];


		 			
	 		 		acc = new AccessLog(method, response, referer, resource, user_agent, user_id);

	 		 		log = new Logs(ipAddress, timestamp, type, size, acc, null, null);
		 			logs.add(log);
	 		 		count++;
		 			
		 			if (count % 500000 == 0)
		 			{
		 				this.logsRepository.saveAll(logs);
		 				logs = new ArrayList<Logs>();
		 			}
	 			}
	 		 	catch (Exception e) {
	 		 		e.printStackTrace();
	 		 	}
	 		}
			sc.close();
			if (logs.size()>0)
				this.logsRepository.saveAll(logs);
			return 0;
	 	}
	 	catch (Exception e) {
	 		e.printStackTrace();
	 		return 1;
	 	}
	}
	
	
	
	private Integer insert_HDFS_DataXceiver() {
		
		String s=" ", s1;
		Integer size, count=0, count2=0;
		File file = new File("C:\\Users\\User\\Documents\\workspace-spring-tool-suite-4-4.3.2.RELEASE\\M149-2\\files\\HDFS_DataXceiver.log"); 

		List<Logs> logs = new ArrayList<Logs>();
		Block b;
		Destination d;
		Logs log;
		List<Block> block;
		List<Destination> destination;

	 	try{
	 		Scanner sc = new Scanner(file);
	 		String[] parts, parts2;
	 		String source_ip, type;
	 		while (sc.hasNextLine())
	 		{
	 			s1 = sc.nextLine();
	 			if(s1.contains("exception") || s1.contains("Exception") || s1.contains("EXCEPTION"))
	 				continue;
 	 			s=s1;
 	 			try {
 	 				block = new ArrayList<Block>();
 	 				destination = new ArrayList<Destination>();
		 			parts = s.split(" ", 2);
		 			parts2 = parts[1].split(" ", 2);
		 			s=parts[0]+" "+parts2[0];

		 			String tempTime=s;

		 			String day= tempTime.substring(0, 2);
		 			String month = tempTime.substring(2, 4);
		 			String year= tempTime.substring(4, 6);
		 			String hours= tempTime.substring(7, 9);
		 			String minutes= tempTime.substring(9, 11);
		 			String seconds= tempTime.substring(11, 13);
					if(year.charAt(0)=='9' || year.charAt(0)=='8' || year.charAt(0)=='7')
							year = "19"+year;
					else
							year="20"+year;


					long timestamp = Long.parseLong((year+getMonthNum(month)+day+hours+minutes+seconds));

		            
		            parts=parts2[1].split(": ", 2);
		            parts=parts[1].split(" ", 2);

		            if(parts[0].charAt(0)<'0' || parts[0].charAt(0)>'9') {
		            	type=parts[0];
		            	parts=parts[1].split("blk_", 2);
		            	parts=parts[1].split(" ", 2);
		            	b=new Block(parts[0]);
		            	block.add(b);
		            	parts=parts[1].split("src: /", 2);
		            	parts=parts[1].split(" ", 2);
			            parts2=parts[0].split(":", 2);
			            source_ip = parts2[0];
		            	parts=parts[1].split("dest: /", 2);
		            	parts=parts[1].split(" ", 2);
    		            parts2=parts[0].split(":", 2);
    		            d = new Destination(parts2[0]);
    		            destination.add(d);
		            	if(parts.length>1) {
		            		parts=parts[1].split("of size ", 2);
		            		size=Integer.parseInt(parts[1]);
		            	}
		            	else
		            		size = null;
		            }
		            else
		            {
			            parts2=parts[0].split(":", 2);
			            source_ip=parts2[0];
		            	parts=parts[1].split(" ", 2);
		            	type = parts[0];
		            	parts=parts[1].split("blk_", 2);
		            	parts=parts[1].split(" ", 2);
		            	b = new Block(parts[0]);
		            	block.add(b);
		            	parts=parts[1].split("to /", 2);
		            	parts=parts[1].split(" ", 2);
    		            parts=parts[0].split(":", 2);
    		            d = new Destination(parts2[0]);
		            	if(parts.length>1) {
		            		parts=parts[1].split("of size ", 2);
		            		size=Integer.parseInt(parts[1]);
		            	}
		            	else
		            		size = null;
		            }
		        	log = new Logs(source_ip, timestamp, type, size, null, destination, block);
				 	
		 			logs.add(log);

		 			count++;
		 			if (count % 500000 == 0) {
		 				this.logsRepository.saveAll(logs);
		 				logs = new ArrayList<Logs>();
		 			}
	 			}
	 		 	catch (Exception e) {
	 		 		count2++;
	 		 	}
	 		}
	 		sc.close();
			if (logs.size()>0)
				this.logsRepository.saveAll(logs);
			if (count2>0)
				System.out.println("Problem with " + count2 + " DataXceivers");
	 		return 0;
	 	}
	 	catch (Exception e) {
	 		e.printStackTrace();
	 		return 1;
		}
	}

	
	
	
	
	
	
	private Integer insert_HDFS_FS_Namesystem() {
		String s=" ", s1;
		Integer count=0;
		File file = new File("C:\\Users\\User\\Documents\\workspace-spring-tool-suite-4-4.3.2.RELEASE\\M149-2\\files\\HDFS_FS_Namesystem.log"); 

		List<Logs> logs = new ArrayList<Logs>();
		Block b;
		Destination d;
		Logs log;
		List<Block> block;
		List<Destination> destination;

		try{
	 		String[] parts, parts2, parts3;
	 		Scanner sc = new Scanner(file);
	 		String source_ip, type;
	 		while (sc.hasNextLine()){
	 			s1 = sc.nextLine();
 
	 			block = new ArrayList<Block>();
 				destination = new ArrayList<Destination>();

 				s=s1;
 	 			parts = s.split(" ", 2);
	 			parts2 = parts[1].split(" ", 2);
	 			s=parts[0]+" "+parts2[0];

	 			String tempTime=s;

	 			String day= tempTime.substring(0, 2);
	 			String month = tempTime.substring(2, 4);
	 			String year= tempTime.substring(4, 6);
	 			String hours= tempTime.substring(7, 9);
	 			String minutes= tempTime.substring(9, 11);
	 			String seconds= tempTime.substring(11, 13);

	 			if(year.charAt(0)=='9' || year.charAt(0)=='8' || year.charAt(0)=='7')
					year = "19"+year;
				else
					year="20"+year;
				
	 			long timestamp = Long.parseLong((year+getMonthNum(month)+day+hours+minutes+seconds));
		
	            parts=parts2[1].split("ask ", 2);
	            parts=parts[1].split(" to ", 2);
	            parts2=parts[0].split(":", 2);
            	source_ip=parts2[0];
	            parts=parts[1].split(" ", 2);
	            type = parts[0];

	            parts=parts[1].split(" to datanode\\(s\\) ", 2);

            	parts2=parts[0].split(" ", 2);
	            while (parts2.length>1) {
	            	parts3=parts2[0].split("blk_",2);
	            	if(parts3.length>1) {
	            		b = new Block(parts3[1]);
	            		block.add(b);
	            	}
	            	parts2=parts2[1].split(" ",2);
	            }
            	parts3=parts2[0].split("blk_",2);
        		b = new Block(parts3[1]);
        		block.add(b);
        		if(parts.length>1) {
                	parts2=parts[1].split(" ", 2);
    	            while (parts2.length>1) {
    		            parts3=parts2[0].split(":", 2);
    		            d = new Destination(parts3[0]);
    		            destination.add(d);
    	            	parts2=parts2[1].split(" ",2);
    	            }
		            parts3=parts2[0].split(":", 2);
		            d = new Destination(parts3[0]);
		            destination.add(d);
            	}

        		log = new Logs(source_ip, timestamp, type, null, null, destination, block);
	 			logs.add(log);

		 		count++;
	 			if (count % 500000 == 0) {
	 				this.logsRepository.saveAll(logs);
	 				logs = new ArrayList<Logs>();
	 			}
	 		}
			if (logs.size()>0)
				this.logsRepository.saveAll(logs);
	 		sc.close();
 			return 0;
	 	}
	 	catch (Exception e) {
	 		e.printStackTrace();
	 		return 1;
		}
	}

	String getMonthNum(String month){
		if(month.equals("Jan"))
			return "01";
		if(month.equals("Feb"))
			return "02";
		if(month.equals("Mar"))
			return "03";
		if(month.equals("Apr"))
			return "04";
		if(month.equals("May"))
			return "05";
		if(month.equals("Jun"))
			return "06";
		if(month.equals("Jul"))
			return "07";
		if(month.equals("Aug"))
			return "08";
		if(month.equals("Sep"))
			return "09";
		if(month.equals("Oct"))
			return "10";
		if(month.equals("Nov"))
			return "11";
		if(month.equals("Dec"))
			return "12";
		return month;
	}
}