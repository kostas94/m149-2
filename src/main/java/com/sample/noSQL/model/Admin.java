package com.sample.noSQL.model;

import java.util.List;
import java.util.Locale;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.github.javafaker.Faker;

@Document (collection = "Admins")
public class Admin {

		@Id
		private String _id;
		@Indexed(unique=true, sparse=true)
		private String username;
		private String email;
		private String phone;
		private List<ObjectId> logs_ids;

		
		public Admin(){
			
		}
		
		public Admin(List<ObjectId> ids){
			
			Faker faker = new Faker(new Locale("en"));

			this.username = faker.name().username();
			this.email = faker.internet().emailAddress(username);
			this.phone = faker.phoneNumber().cellPhone();
			this.logs_ids = ids;
		}
		
		
		
		public String getId() {
			return _id;
		}
		
		public String getUsername() {
			return username;
		}
		
		public String getEmail() {
			return email;
		}
		
		public String getPhone() {
			return phone;
		}
		
		public List<ObjectId> getLogsids() {
			return logs_ids;
		}




		public void setId(String id) {
			this._id=id;
		}
		
		public void getUsername(String username) {
			this.username = username;
		}
		
		public void setEmail(String email) {
			this.email = email;
		}
		
		public void getPhone(String phone) {
			this.phone = phone;
		}
		
		public void setLogsids(List<ObjectId> l) {
			this.logs_ids = l;
		}

		
}
