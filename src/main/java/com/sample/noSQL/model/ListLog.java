package com.sample.noSQL.model;

import java.util.ArrayList;
import java.util.List;

public class ListLog {
	private List<Logs> list;
	
	public ListLog() {
		list = new ArrayList<Logs>();
	}
	
	public List<Logs> getList(){
		return list;
	}
	
	public void setList(List<Logs> list) {
		this.list = list;
	}
}
