package com.sample.noSQL.model;


public class Block {
	private String block_id;

	public Block(String block_id) {
			this.block_id = block_id;
		}
	public Block() {}

	public String getBlockId() {
		return block_id;
	}

	public void setBlockId(String id) {
		this.block_id = id;
	}
}
