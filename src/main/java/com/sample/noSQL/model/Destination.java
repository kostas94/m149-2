package com.sample.noSQL.model;


public class Destination {
	private String destination_ip;

	
	public Destination(String destination_ip) {
		this.destination_ip = destination_ip;
	}

	public Destination() {};
	
	public String getDestinationIp() {
		return destination_ip;
	}

	
	public void setDestinationIp(String ip) {
		this.destination_ip=ip;
	}

}
