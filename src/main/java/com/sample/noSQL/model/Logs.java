package com.sample.noSQL.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document (collection = "Logs")
public class Logs {

	@Id
	private String _id;
	@Indexed(direction = IndexDirection.ASCENDING)
	private String ipAddress;
	private String type;
	private Integer size;
	@Indexed(direction = IndexDirection.ASCENDING)
	private long timestamp;
	private AccessLog access;
	private List<Destination> destinations;
	private List<Block> blocks;
	
	public Logs() {
	}
	
	public Logs(String ipAddress, long timestamp, String type, Integer size, AccessLog access, List<Destination> destinations, List<Block> blocks){
		this.ipAddress = ipAddress;
		this.timestamp = timestamp;
		this.type = type;
		this.size = size;
		this.access = access;
		this.destinations=destinations;
		this.blocks = blocks;
	}
	
	public String getId() {
		return _id;
	}
	
	public String getIpAddress(){
		return ipAddress;
	}
	
	public long getTimestamp(){
		return timestamp;
	}
	
	public String getType() {
		return type;
	}
	
	public Integer getSize() {
		return size;
	}
	
	public AccessLog getAccess() {
		return access;
	}
	
	public List<Destination> getDestinations(){
		return destinations;
	}
	
	public List<Block> getBlocks(){
		return blocks;
	}
	
	
	public void setId(String id) {
		this._id = id;
	}
	
	public void setIpAddress(String ipAddress){
		this.ipAddress = ipAddress;
	}
	
	public void setTimestamp(long date) {
		this.timestamp = date;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setSize(Integer size) {
		this.size = size;
	}
	
	public void setAccess(AccessLog access) {
		this.access = access;
	}
	
	public void setDestinations(List<Destination> d){
		this.destinations = d;
	}
	
	public void setBlocks(List<Block> b){
		this.blocks = b;
	}
}
