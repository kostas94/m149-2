package com.sample.noSQL.model;

public class AccessLog {
	private String method;
	private Integer responsestatus;
	private String referer;
	private String resourcerequested;
	private String useragent;
	private String userid;

	public AccessLog() {};

	public AccessLog(String method, Integer response_status, String referer, String resource_requested, String user_agent, String user_id) {
		this.method = method;
		this.responsestatus = response_status;
		this.referer = referer;
		this.resourcerequested = resource_requested;
		this.useragent = user_agent;
		this.userid = user_id;
	}

	public String getMethod() {
		return method;
	}

	public Integer getResponsestatus() {
		return responsestatus;
	}
	
	public String getReferer() {
		return referer;
	}
	
	public String getResourcerequested() {
		return resourcerequested;
	}
	
	public String getUseragent() {
		return useragent;
	}
	
	public String getUserid() {
		return userid;
	}



	public void setMethod(String method) {
		this.method = method;
	}

	public void setResponsestatus(Integer r) {
		this.responsestatus = r;
	}
	
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public void setResourcerequested(String resource) {
		this.resourcerequested = resource;
	}
	
	public void setUseragent(String agent) {
		this.useragent = agent;
	}
	
	public void setUserid(String id) {
		this.userid = id;
	}

}