package com.sample.noSQL.Repository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sample.noSQL.model.Admin;

@Repository
public interface AdminRepository extends MongoRepository<Admin, String>{
}
