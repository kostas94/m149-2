package com.sample.noSQL.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.sample.noSQL.model.Logs;

@Repository
public interface LogsRepository extends MongoRepository<Logs, String>{
	
}
