package com.sample.noSQL.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.sample.noSQL.Repository.LogsRepository;
import com.sample.noSQL.model.ListLog;
import com.sample.noSQL.model.Logs;

@RestController
@RequestMapping("/logs")
public class LogsController {

	private LogsRepository logsRepository;

	
	public LogsController(LogsRepository logsRepository) {
		this.logsRepository = logsRepository;
	}

	

	
	@GetMapping("/all")
	public List<Logs> getAll(){
		List<Logs> logs = this.logsRepository.findAll();
		return logs;
	}

	
	
	@GetMapping("question1/{from}.{to}")
	public List<List<String>> question1(@PathVariable("from") String from, @PathVariable("to") String to){

			MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
			MongoClient mongoClient = new MongoClient(connectionString);
			MongoDatabase database = mongoClient.getDatabase("LogsDb");
			MongoCollection<Document> collection = database.getCollection("Logs");

			long fromtimestamp = dateCalculator(from,6);
			long totimestamp = dateCalculator(to,6);

			

			AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
					new Document("$match", new Document("timestamp", new Document("$gte", fromtimestamp).append("$lte", totimestamp))),
					new Document("$group", new Document("_id", "$type").append("logs_count", new Document("$sum", 1))),
					new Document("$sort", new Document("logs_count", -1))
					));

			
			List<List<String>> list = new ArrayList<List<String>>();
			for (Document dbObject: aggIterable )
			{
				List<String> temp = new ArrayList<String>();
				temp.add(dbObject.get("_id").toString());
				temp.add(dbObject.get("logs_count").toString());
				list.add(temp);
			}
				
			mongoClient.close();
			return list;
	}

	
	
	
	@GetMapping("question2/{type}/{from}.{to}")
	public List<List<String>> question2(@PathVariable("type") String type, @PathVariable("from") String from, @PathVariable("to") String to){

			long fromdate= dateCalculator(from,6);
        	long todate= dateCalculator(to,6);
        	
        	MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
			MongoClient mongoClient = new MongoClient(connectionString);
			MongoDatabase database = mongoClient.getDatabase("LogsDb");
			MongoCollection<Document> collection = database.getCollection("Logs");
			
			
			List<Object> takeday= new ArrayList<Object>();
			takeday.add("$timestamp");
			takeday.add(-6);
			
			AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
					new Document("$match", new Document("timestamp", new Document("$gte", fromdate)
							.append("$lte", todate))
					.append("type", type)),
					new Document("$group", new Document("_id", new Document("$trunc", takeday)).append("logs_count", new Document("$sum", 1))),
					new Document("$sort", new Document("_id", 1))));

			
			List<List<String>> list = new ArrayList<List<String>>();
			for (Document dbObject: aggIterable )
			{
				List<String> temp = new ArrayList<String>();
				temp.add(dbObject.get("_id").toString().substring(0, 4) + "-" +dbObject.get("_id").toString().substring(4, 6)+ "-"+dbObject.get("_id").toString().substring(6, 8));
		 		temp.add(dbObject.get("logs_count").toString());
				list.add(temp);
			}
			mongoClient.close();
			return list;
        }
	

	
	@GetMapping("question3/{day}")
	public List<Object> question3(@PathVariable("day") String date){

		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");

		
		long timestamp = dateCalculator(date,3);

		List<Object> bla = new ArrayList<Object>();
		bla.add("$most_common_logs");
		bla.add(3);

		List<Object> bla2 = new ArrayList<Object>();
		bla2.add("$timestamp");
		bla2.add(-6);

			AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
					new Document("$project", new Document("timestamp", new Document("$trunc", bla2)).append("ipAddress", "$ipAddress").append("type", "$type")),
					new Document("$match", new Document("timestamp", timestamp)),
					new Document("$group", new Document("_id", new Document("ipAddress", "$ipAddress").append("type", "$type")).append("total_type_logs_on_address", new Document("$sum", 1))),
					new Document("$sort", new Document("_id.ipAddress", -1).append("total_type_logs_on_address", -1)),
					new Document("$group", new Document("_id", "$_id.ipAddress").append("most_common_logs", new Document("$push", "$_id.type"))),
					new Document("$project", new Document("most_common_logs", new Document("$slice", bla)))));
			
			List<Object> list = new ArrayList<Object>();
			for (Document dbObject: aggIterable )
				list.add(dbObject);

			mongoClient.close();
			return list;
        }

	
	
		
	@GetMapping("question4/{from}.{to}")
	public List<Object> question4(@PathVariable("from") String from, @PathVariable("to") String to){

			long fromdate= dateCalculator(from, 6);
        	long todate= dateCalculator(to, 6);
        	
        	
        	MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
			MongoClient mongoClient = new MongoClient(connectionString);
			MongoDatabase database = mongoClient.getDatabase("LogsDb");
			MongoCollection<Document> collection = database.getCollection("Logs");
			
			
			List<Object> takeday= new ArrayList<Object>();
			takeday.add("$timestamp");
			takeday.add(-6);
			
			AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
					new Document("$match", new Document("timestamp", new Document("$gte", fromdate)
							.append("$lte", todate))
					.append("access.method", new Document("$ne", null))),
					new Document("$group", new Document("_id", "$access.method").append("methods_count", new Document("$sum", 1))),
					new Document("$sort", new Document("methods_count", 1)),
					new Document("$limit", 2),
					new Document("$project", new Document("_id", "$_id"))));

			
			List<Object> list = new ArrayList<Object>();
			for (Document dbObject: aggIterable )
				list.add(dbObject);
			mongoClient.close();
			return list;
        }
	
	
	
	
	
	
	@GetMapping("question5")
	public List<String> question5(){
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");
	

		
		AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
				new Document("$match", new Document("access.referer", new Document("$ne", null))),
				new Document("$group", new Document("_id", new Document( "referer", "$access.referer")
						.append("resource", "$access.resourcerequested"))),
				new Document("$group", new Document("_id", "$_id.referer")
						.append("resource_count", new Document("$sum", 1))),
				new Document("$match", new Document("resource_count", new Document("$gte", 2))),
				new Document("$project", new Document("_id", "$_id"))));

		List<String> list = new ArrayList<String>();
		for (Document dbObject : aggIterable)
		{
			list.add(dbObject.get("_id").toString());
		}

		mongoClient.close();
		return list;
	}
			
	
	
	
	@GetMapping("question6")
	public List<String> question6(){
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");
	

		List<String> bla = new ArrayList<String>();
		
		bla.add("replicate");
		bla.add("Served");

		List<Object> takeday= new ArrayList<Object>();
		takeday.add("$timestamp");
		takeday.add(-6);

		AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
				new Document("$unwind", "$blocks"),
				new Document("$match", new Document("type", new Document("$in", bla))),
				
				new Document("$group", new Document("_id", new Document("date", new Document("$trunc", takeday)).append("type", "$type")
						.append("block", "$blocks.block_id"))),
				new Document("$group", new Document("_id", new Document("date", "$_id.date")
							.append("block", "$_id.block"))
						.append("count", new Document("$sum", 1))),
				new Document("$match", new Document("count", new Document("$gte", 2))),
				
				new Document("$group", new Document("_id", "$_id.block"))));

		
		List<String> list = new ArrayList<String>();
		for (Document dbObject : aggIterable)
			list.add(dbObject.get("_id").toString());

		mongoClient.close();
		return list;
	}

	
	@GetMapping("question7/{day}")
	public List<Object> question7(@PathVariable("day") String date){

		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");

		long timestamp = dateCalculator(date, 3);

		List<Object> takedate = new ArrayList<Object>();
		takedate.add("$timestamp");
		takedate.add(-6);

			AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
					new Document("$project", new Document("id", "$id").append("date", new Document("$trunc", takedate))),
					new Document("$match", new Document("date", timestamp)),
					new Document("$lookup", new Document("from","Admins")
							.append("localField", "_id")
							.append("foreignField", "logs_ids")
							.append("as", "list_of_logs")),
					new Document("$project", new Document("_id", "$_id").append("upvotersCount", new Document("$size","$list_of_logs._id"))),
					new Document("$sort", new Document("upvotersCount", -1)),
					new Document("$limit", 50)));
			
			
			List<Object> list = new ArrayList<Object>();
			for (Document dbObject: aggIterable ) {
				list.add(dbObject.get("_id").toString());
//				List<Object> templist=new ArrayList<Object>();
//				templist.add(dbObject.get("_id").toString());
//				templist.add(dbObject.get("upvotersCount").toString());
//				list.add(templist);
			}
			mongoClient.close();
			return list;
        }

	
	
	
	
	
	
	@GetMapping("question8")
	public List<String> question8(){
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Admins");
	

		AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
				new Document("$project", new Document("_id", "$username")
						.append("countOfUpvotes", new Document("$cond", new Document("if", new Document("isArray", "$logs_ids"))
								.append("then", new Document("$size", "$logs_ids"))
								.append("else", "NA")))),
				new Document("$sort", new Document("countOfUpvotes", -1)),
				new Document("$limit", 50)
				));
		
		List<String> list = new ArrayList<String>();
		for (Document dbObject : aggIterable)
		{
			list.add(dbObject.get("_id").toString());
		}

		mongoClient.close();
		return list;
	}


	
	
	
	@GetMapping("question9")
	public List<Object> question9(){
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Admins");
	

		List<Object> list = new ArrayList<Object>();
		list.add("$list_of_logs.ipAddress");
		list.add(new ArrayList<Object>());
		
		AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
				new Document("$project", new Document("username", "$username").append("logs_ids", "$logs_ids")),
				new Document("$lookup", new Document("from","Logs")
						.append("localField", "logs_ids")
						.append("foreignField", "_id")
						.append("as", "list_of_logs")),
				new Document("$project", new Document("_id", "$username")
						.append("ipAddressesCount", new Document("$size", new Document("$setDifference", list)))),
				new Document("$sort", new Document("ipAddressesCount", -1)),
				new Document("$limit", 50)
				));

		
		List<Object> output = new ArrayList<Object>();
		for (Document dbObject : aggIterable)
		{
			output.add(dbObject);
		}

		mongoClient.close();
		return output;
	}

	
	
	
	
	@GetMapping("question10")
	public List<Object> question10(){
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Admins");
	
		AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
				new Document("$project", new Document("_id", "$email").append("logs_ids", "$logs_ids")),
				new Document("$unwind", new Document("path", "$logs_ids")),
				new Document("$group", new Document("_id", new Document("log", "$logs_ids").append("email", "$_id")).append("count", new Document("$sum", 1))),
				new Document("$match", new Document("count", new Document("$gte", 2))),
				new Document("$project", new Document("_id", "$_id.log")))).allowDiskUse(true);
		

		
		List<Object> output = new ArrayList<Object>();
		for (Document dbObject : aggIterable)
		{
			output.add(dbObject.get("_id").toString());
		}

		mongoClient.close();
		return output;
	}	
	
	
	
	@GetMapping("question11/{username}")
	public List<String> question11(@PathVariable("username") String username){
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Admins");
	

		List<Object> bla = new ArrayList<Object>();
		
		bla.add("replicate");
		bla.add(0);
		AggregateIterable<Document> aggIterable = collection.aggregate(Arrays.asList(
				new Document("$match", new Document("username", username)),
				new Document("$lookup", new Document("from","Logs")
						.append("localField", "logs_ids")
						.append("foreignField", "_id")
						.append("as", "list_of_logs")),
				new Document("$project", new Document("blocks", "$list_of_logs.blocks")),
				new Document("$unwind", new Document("path", "$blocks")),
				new Document("$unwind", new Document("path", "$blocks")),
				new Document("$group", new Document("_id", "$blocks.block_id"))
		));

		
		List<String> list = new ArrayList<String>();
		for (Document dbObject : aggIterable)
		{
			list.add(((dbObject.get("_id"))).toString());
		}
		mongoClient.close();
		return list;
	}

		
	
	
	
	
	
	@RequestMapping("/insert")
	public void insert(@Valid @RequestBody Logs log) {
		this.logsRepository.save(log);
	}
	
	@RequestMapping("/insertMany")
	public void insertMany(@Valid @RequestBody ListLog log) {
		this.logsRepository.saveAll(log.getList());
	}
	
	
	@RequestMapping("/upvote/{username}")
	public String upvote(@PathVariable("username") String username, @Valid @RequestBody ListLog log) {
		List<Logs> logs = log.getList();
		MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017");
		MongoClient mongoClient = new MongoClient(connectionString);
		MongoDatabase database = mongoClient.getDatabase("LogsDb");
		MongoCollection<Document> collection = database.getCollection("Logs");

		MongoCollection<Document> collection2 = database.getCollection("Admins");

		AggregateIterable<Document> aggIterable = collection2.aggregate(Arrays.asList(
				new Document("$match", new Document("username", username))));


		
		if (aggIterable.first()==null) {
			mongoClient.close();
			return "Wrong Username";
		}
		
		for(Logs i: logs) {
			try {
				aggIterable = collection.aggregate(Arrays.asList(
						new Document("$match", new Document("_id", new ObjectId(i.getId())))));
				if (aggIterable.first()!= null)
					collection2.updateOne(new Document("username", username), new Document("$addToSet", new Document("logs_ids", aggIterable.first().get("_id"))));
			}
			catch (Exception e) {
				continue;
			}
		}
		mongoClient.close();
		return "Upvote Ends";
	}


	
	
	public long dateCalculator(String date, int i) {
		String year = "0000", month = "01", day="01", hour="00", minute="00", second="00";
		String[] parts;

		parts=date.split("-",2);
 		year=parts[0];
 	   	if(parts.length>1 && i>=2) {
 			parts=parts[1].split("-", 2);
 			month=parts[0];
			if(parts.length>1 && i>=3) {
	    		parts=parts[1].split(",", 2);
        		day=parts[0];
	        	if(parts.length>1 && i>=4) {
	        		parts=parts[1].split(":", 2);
            		hour=parts[0];
	            	if(parts.length>1 && i>=5) {
	            		parts=parts[1].split(":", 2);
                		minute=parts[0];
	                	if(parts.length>1 && i>=6) {
	                    		second=parts[1];
                    	}
                	}
            	}
        	}
	   	}


		if(month.length() == 1)	month="0" + month;
		if(day.length() == 1) day="0" + day;
		if(hour.length() == 1) hour="0" + hour;
		if(minute.length() == 1) minute = "0" + minute;
		if(second.length() == 1) second = "0" + second;

		
		
		long timestamp = Long.parseLong(year+month+day+hour+minute+second);
		return timestamp;
	}
}